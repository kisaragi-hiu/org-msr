;;; org-msr.el --- Minimal Spaced Repetition  -*- lexical-binding: t -*-
;;
;; Author: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; Version: 0.11.0
;; Package-Requires: ((emacs "25.1") (org "9.2") (dash "2.16.0") (s "1.12.0"))
;; Homepage: https://kisaragi-hiu.com/projects/org-msr
;; Keywords: convenience org
;;
;; This program is not part of GNU Emacs.
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.
;;; Commentary:
;; A minimal (manual) spaced repetition setup using Org, repeating
;; schedules, and todo keywords.

;;; Code:
(require 'cl-lib)
(require 'org)
(require 'dash)
(require 's)
(require 'dom) ; for org-msr-insert-word-definition
(eval-when-compile
  (require 'pcase)
  (require 'message))

;;;; Customize
(defgroup org-msr nil
  "Minimal spaced repetition setup in Org."
  :group 'org
  :prefix "org-msr-")

(defcustom org-msr-keyword-frequency-alist
  ;; The "/1d" "1m" suffixes are not magic, they simply help you
  ;; choose a new frequency in `org-todo'.
  '(("DAILY/1d" . "1d")
    ("HARD/3d" . "3d")
    ("UNFAMILIAR/1w" . "1w")
    ("SOMEWHAT/2w" . "2w")
    ("FAMILIAR/1m" . "1m")
    ("EASY/6m" . "6m")
    ("CONFIDENT/1y" . "1y")
    ("MEMORIZED/3y" . "3y"))
  "Alist mapping TODO keywords in the memory file to repeater frequencies.

\"never\" is a special frequency that tells Org-msr to not attach a repeater."
  :group 'org-msr
  :type '(alist :key-type string :value-type string))

(defcustom org-msr-setup-heading-name "Org-msr Setup"
  "Heading name for Org-msr setup code."
  :group 'org-msr
  :type 'string)

;;;; Utility functions
(defun org-msr--refresh-org ()
  "Refresh Org exactly like what \\[org-ctrl-c-ctrl-c] in a #+TODO line does."
  (let ((org-inhibit-startup-visibility-stuff t)
        (org-startup-align-all-tables nil))
    (when (boundp 'org-table-coordinate-overlays)
      (mapc #'delete-overlay org-table-coordinate-overlays)
      (setq org-table-coordinate-overlays nil))
    (org-save-outline-visibility 'use-markers (org-mode-restart))))

;;;; Core function: Update repeater

;;;###autoload
(defun org-msr-update-repeater (&rest _)
  "Update repeater for each org-msr item based on their familiarity."
  (interactive)
  (save-excursion
    (pcase-dolist (`(,keyword . ,frequency) org-msr-keyword-frequency-alist)
      (setf (point) (point-min))
      (while (search-forward (format "* %s" keyword) nil t)
        (pcase `(,(and (boundp 'org-state)
                       (string= org-state "DONE"))
                 ,(org-get-scheduled-time (point))
                 ,(string= frequency "never"))
          ;; no existing schedule, not "never"
          ;; -> create the scheduler
          (`(,_ nil nil)
           (end-of-line)
           (insert "\n" (format "SCHEDULED: <%s .+%s>"
                                (format-time-string "%Y-%m-%d")
                                frequency)))
          ;; existing schedule, switching to DONE, not "never"
          ;; -> update schedule, throwing away old date
          (`(t ,_ nil)
           (re-search-forward "SCHEDULED: <.* .*>" nil t)
           (replace-match
            (format "SCHEDULED: %s .+%s>"
                    (substring (format-time-string
                                (org-time-stamp-format))
                               0 -1)
                    frequency)))
          ;; existing schedule, not "never" -> update schedule
          (`(,_ ,_ nil)
           (re-search-forward "SCHEDULED: <\\(.*\\) .*>" nil t)
           (replace-match (format "SCHEDULED: <\\1 .+%s>" frequency)))
          ;; existing schedule, "never" -> remove it
          (`(,_ ,_ t)
           (when (re-search-forward "SCHEDULED: <\\(.*\\) .*>" nil t)
             (message-delete-line))))))))

;;;###autoload
(defun org-msr-update-element-at-point (&rest _)
  "Like `org-msr-update-repeater', but only update element at point."
  (save-excursion
    (save-restriction
      (org-narrow-to-element)
      (org-msr-update-repeater))))

;;;; Org-msr mode

(defvar org-msr-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c _") 'org-msr-update-repeater)
    map)
  "Keymap for `org-msr-mode'.")

(defvar org-msr-mode--files nil "List of full paths to files that use `org-msr-mode'.")

;;;###autoload
(define-minor-mode org-msr-mode
  "Org-msr (Minimal Spaced Repetition) minor mode.

Every TODO heading will get a repeating schedule based on their
TODO keyword, as defined in `org-msr-keyword-frequency-alist'.

The repeaters are updated everytime `org-todo' is called.

Every file this mode gets activated in will be added to
`org-msr-mode--files', ready to be used by `org-msr-agenda'
so that you don't have to add Org-msr files to your personal
agenda.

\\<org-msr-mode-map>
\\[org-msr-update-repeater] to explicitly update repeaters.
\\[org-msr-setup] to set up this file for Org-msr, which see.

\\{org-msr-mode-map}"
  :group 'org-msr :init-value nil :lighter " Org-msr"
  :keymap org-msr-mode-map
  (if org-msr-mode
      (progn
        ;; org-todo calls this
        (add-hook 'org-after-todo-state-change-hook #'org-msr-update-element-at-point  nil :local)
        (unless (or (member buffer-file-name org-msr-mode--files)
                    (not buffer-file-name))
          ;; do this when buffer-file-name exists and isn't already saved
          (setq org-msr-mode--files
                (cons buffer-file-name org-msr-mode--files))))
    (remove-hook 'org-after-todo-state-change-hook #'org-msr-update-element-at-point :local)
    (setq org-msr-mode--files (delete buffer-file-name org-msr-mode--files))))

;;;; Commands
;;;###autoload
(defun org-msr-setup ()
  "Set this file up for org-msr.

Will only do anything if a heading named by
`org-msr-setup-heading-name' (default \"Org-msr Setup\") doesn't
already exist.

- Add TODO keyword definitions according to `org-msr-keyword-frequency-alist'
- Tell Emacs to start org-msr-mode in this file"
  (interactive)
  (save-excursion
    (setf (point) (point-min))
    (if (search-forward (concat "* " org-msr-setup-heading-name) nil t)
        (message "%s" "Org-msr is already set up")
      (setf (point) (point-max))
      (insert "\n* " org-msr-setup-heading-name "\n"
              ;; TODO keywords
              (mapconcat (lambda (pair)
                           (format "#+TODO: %s | DONE(d)" (car pair)))
                         org-msr-keyword-frequency-alist
                         "\n"))
      (add-file-local-variable 'eval '(org-msr-mode 1))
      (org-msr--refresh-org)
      (message "%s" "Org-msr has been set up"))))

;;;###autoload
(defun org-msr-agenda (files)
  "Run `org-agenda-list' on FILES that use `org-msr-mode'.

This is to avoid having to add Org-msr files to your
`org-agenda-files'. Doing so would significantly slow down your
Org Agenda, as Org-msr files are expected to contain hundreds, if
not thousands, of scheduled todos.

A \\[universal-argument] can be provided, which `org-agenda-list'
will pick up. See its docstring for details."
  (interactive `(,org-msr-mode--files))
  (let ((org-agenda-files files))
    ;; prefix argument will be passed on through `current-prefix-arg'
    (call-interactively #'org-agenda-list)))

;;;;; Org-msr stats
(require 'org-agenda)

;;;###autoload
(cl-defun org-msr-stats-count (&key parent filter (buffer (current-buffer)))
  "Return number of Org-msr entries in BUFFER (defaults to current).

If PARENT is non-nil, only count children of that heading.

If FILTER is a string, only count entries that contain it.

Interactively,
- ask for BUFFER if `org-msr-mode' is not active or with a \\[universal-argument]
- also ask for PARENT with \\[universal-argument] \\[universal-argument]
- also ask for FILTER with \\[universal-argument] \\[universal-argument] \\[universal-argument]."
  ;; C-u madness
  (interactive (list :parent (and (ignore-errors
                                    (>= (prefix-numeric-value current-prefix-arg) 16))
                                  (read-string "Parent heading: "))
                     :filter (and (ignore-errors
                                    (>= (prefix-numeric-value current-prefix-arg) 64))
                                  (read-string "Filter string: "))
                     :buffer (when (or current-prefix-arg
                                       (not org-msr-mode))
                               (read-buffer "Buffer: "))))
  (when (called-interactively-p 'interactive)
    (cl-return-from org-msr-stats-count
      (message "%s" (org-msr-stats-count :parent parent :filter filter))))
  (save-excursion
    (save-restriction
      (with-current-buffer buffer
        (when (stringp parent)
          (widen)
          (goto-char (point-min))
          (re-search-forward (format "^\\*+ %s$" parent))
          (narrow-to-defun)
          (goto-char (point-min)))
        (let ((todos (org-agenda-get-todos)))
          (if (stringp filter)
              (length (seq-filter
                       (lambda (it)
                         (string-match-p filter it))
                       todos))
            (length todos)))))))

;;;;; inserting definitions

;; a kiji can contain multiple entries, so we have to abstract this
(require 'dom)
(cl-defstruct (org-msr-dictionary-entry
               (:copier nil)
               (:constructor org-msr-dictionary-entry))
  title definition)

(defun org-msr--weblio-kiji? (thing)
  "Is THING a Weblio \"kiji\" item?
kiji entries are divs like this:
'(div ((class \"kiji\")) ...)"
  (ignore-errors
    (equal (dom-attr thing 'class) "kiji")))

(defun org-msr--weblio-kiji-titles (kiji)
  "Get titles in Weblio KIJI."
  (->> (dom-by-class kiji "midashigo")
       ;; we might have more than one match.
       (--map
        (->> (dom-children it)
             -flatten
             (-filter #'stringp)
             (-map #'s-trim)
             (s-join "")))))
(defun org-msr--weblio-kiji-definitions (kiji)
  "Return definitions of the word described by KIJI."
  ;; It proved to be quite difficult to split paragraphs from Weblio's
  ;; markup. Use the number of separators to determine paragraphs
  ;; instead. Here I use a character that Weblio probably (definitely)
  ;; won't use to avoid conflict.
  (let ((sep (string 1234567))) ; elisp numbers are characters.
    (-some->> (dom-by-class kiji (rx (or "NetDicBody"
                                         "Nhgkt"
                                         "Jtnhj"
                                         "Sgkdj")))
              (--map (->> (dom-texts it sep)
                          (s-replace-regexp
                           (format "%s %s+" sep sep) "\n")
                          (s-replace
                           sep "")
                          s-trim)))))
(defun org-msr--weblio-kiji-to-entry (node)
  "Turn a weblio kiji class NODE into abstract entries."
  (cl-mapcar
   (lambda (title definition)
     (org-msr-dictionary-entry :title title :definition definition))
   (org-msr--weblio-kiji-titles node)
   (org-msr--weblio-kiji-definitions node)))
(defun org-msr-retrieve-definition-weblio (word)
  "Retrieve definitions of WORD from Weblio."
  (let ((buf (url-retrieve-synchronously
              (format "https://www.weblio.jp/content_find?query=%s" word))))
    (with-current-buffer buf
      (decode-coding-region (point-min) (point-max) 'utf-8)
      (save-excursion
        (goto-char (point-min))
        (search-forward "\n\n")
        (let* ((document (libxml-parse-html-region (point) (point-max))))
          (->> (dom-by-class document "^kiji$")
               (-map #'org-msr--weblio-kiji-to-entry)
               -flatten))))))

(defun org-msr-insert-word-definition (word)
  "Insert definition of WORD.
Currently only Japanese definitions on Weblio is supported."
  (interactive "MWord: ")
  (cl-block not-found
    (let ((entries (org-msr-retrieve-definition-weblio word))
          index entry)
      ;; choosing an entry
      ;; TODO: what if I want all entries?
      (pcase (length entries)
        (0 (message "No definition found for %s" word)
           (cl-return-from not-found))
        (1 (setq index 0))
        (_
         (let ((entries-for-display
                ;; I'm fascinated by this syntax. Seriously.
                (cl-loop
                 for entry being the elements of entries using (index i)
                 collect
                 (format
                  "%s: %s: %s"
                  ;; this is so that display and actual value can deviate
                  ;; without having to use helm or ivy
                  i
                  (org-msr-dictionary-entry-title entry)
                  (->> (org-msr-dictionary-entry-definition entry)
                       (s-replace "\n" " ")
                       (s-truncate (/ (frame-width) 2)))))))
           (--> (completing-read "Which one?: " entries-for-display nil t)
                (s-match (rx bol (submatch (1+ digit)) ":") it)
                cadr
                string-to-number
                (setq index it)))))
      (setq entry (elt entries index))
      ;; don't repeat it
      (unless (equal (org-msr-dictionary-entry-title entry) word)
        (insert (org-msr-dictionary-entry-title entry) "\n"))
      (insert (org-msr-dictionary-entry-definition entry) "\n"))))
;;;###autoload
(defun org-msr-insert-current-heading-definition ()
  "Insert definition for the current heading."
  (interactive)
  (save-excursion
    (end-of-line)
    (insert "\n")
    (org-msr-insert-word-definition
     (org-entry-get nil "ITEM"))))

;;;; Provide
(provide 'org-msr)
;;; org-msr.el ends here
